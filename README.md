# WiFi Clock UI

User interface for [WiFi Clock](https://github.com/strange-v/wifi_clock).

## Dev server

Run node `fake-api.js` to start dummy back-end API, then run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build --prod=true --extractCss=true --aot=true --buildOptimizer=true` to build the project. The build artifacts will be stored in the `dist/` directory..
