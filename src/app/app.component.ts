import { Component, OnInit, ViewChild } from '@angular/core';
import { SettingService } from './services/setting.service';
import { Settings } from './models/Settings';
import { TabsComponent } from './components/tabs/tabs.component';
import { Time } from './models/Time';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild(TabsComponent)
  private form: TabsComponent;

  title = 'WiFi Clock Settings';
  settings: Settings;
  loading: boolean;
  error: string;
  errorMessage: string;

  constructor(private settingService: SettingService) { }

  ngOnInit(): void {
    this.error = '';
    this.loading = true;
    this.settingService.getSettings()
      .subscribe(
        settings => {
          this.loading = false;
          this.settings = settings;
        },
        error => {
          debugger
          this.loading = false;
          this.error = error;
        });
  }

  save() {
    if (!this.form.isValid())
      return;

    const values = this.form.getValues();
    const data: Settings = new Settings;

    data.system.wifiSSID = values.wifiSsid.trim();
    data.system.wifiPwd = values.wifiPwd.trim();
    data.system.authUser = values.authUser.trim();
    data.system.authPwd = values.authPwd.trim();
    data.system.otaPwd = values.otaPwd.trim();

    data.time.ntpUrl = values.ntpUrl.trim();
    data.time.syncPeriod = values.syncPeriod;
    data.time.offset = values.offset;
    data.time.useDST = values.useDST;
    data.time.startDST = values.startDST;
    data.time.endDST = values.endDST;

    data.display.leadingZero = values.leadingZero;
    data.display.blinkColumn = values.blinkColumn;
    data.display.doNotBlink = values.doNotBlink;
    data.display.dnbFrom = this.getTime(values.dnbFrom);
    data.display.dnbTo = this.getTime(values.dnbTo);
    data.display.minBrightness = values.minBrightness;
    data.display.maxBrightness = values.maxBrightness;

    this.error = '';
    this.loading = true;
    this.settingService.saveSettings(data)
      .subscribe(
        success => {
          this.loading = false;
          this.settings = data;
        },
        error => {
          this.loading = false;
          this.error = error.error || error.statusText || error;
        });
  }

  getTime(value: string): Time {
    const time = new Time;
    const values = value.split(':');

    time.hour = parseInt(values[0], 10);
    time.minute = parseInt(values[1], 10);

    return time;
  }
}
