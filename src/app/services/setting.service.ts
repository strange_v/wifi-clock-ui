import { Injectable } from '@angular/core';
import { HttpClient, } from '@angular/common/http';
import { Response } from 'src/app/models/Response';
import { Settings } from 'src/app/models/Settings';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SettingService {
  private settingsUrl = '/api/config';

  constructor(private http: HttpClient) { }

  public getSettings(): Observable<Settings> {
    return this.http.get<Response<Settings>>(this.settingsUrl)
      .pipe(map(rsp => {
        if (rsp.code == 0) {
          return rsp.entity;
        }
        throw `Error code: ${rsp.code}`;
      }));
  }

  public saveSettings(settings: Settings): Observable<boolean> {
    const data = JSON.parse(JSON.stringify(settings));

    this._fixPasswords(data);
    return this.http.post<Response<void>>(this.settingsUrl, data)
      .pipe(map(rsp => {
        if (rsp.code == 0) {
          return true;
        }
        throw `Error code: ${rsp.code}`;
      }));
  }

  private _fixPasswords(settings: Settings) {
    settings.system.wifiPwd = settings.system.wifiPwd.trim();
    settings.system.authPwd = settings.system.authPwd.trim();
    settings.system.otaPwd = settings.system.otaPwd.trim();
  }
}
