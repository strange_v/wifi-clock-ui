import { Component, OnInit, Input } from '@angular/core';
import { TimeSettings } from '../../models/TimeSettings';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-time-settings',
  templateUrl: './time-settings.component.html',
  styleUrls: ['./time-settings.component.scss']
})
export class TimeSettingsComponent implements OnInit {
  @Input()
  data: TimeSettings;
  @Input()
  public parent: FormGroup;

  constructor() { }

  ngOnInit() {
    this.parent.registerControl('ntpUrl', new FormControl(this.data.ntpUrl, Validators.required));
    this.parent.registerControl('syncPeriod', new FormControl(this.data.syncPeriod, Validators.compose([
      Validators.required,
      Validators.min(1 * 60),
      Validators.max(7 * 24 * 60),
    ])));
    this.parent.registerControl('offset', new FormControl(this.data.offset, Validators.compose([
      Validators.required,
      Validators.min(-12 * 60),
      Validators.max(12 * 60),
    ])));
    this.parent.registerControl('useDST', new FormControl(this.data.useDST));
    this.parent.registerControl('startDST', new FormControl(this.data.startDST, Validators.required));
    this.parent.registerControl('endDST', new FormControl(this.data.endDST, Validators.required));
  }

  onUseDstChange() {
    const state = this.parent.controls['useDST'].value ? 'enable' : 'disable';
    
    this.parent.controls['startDST'][state]();
    this.parent.controls['endDST'][state]();
  }
}
