import { Component, Input, OnInit } from '@angular/core';
import { Info } from '../../models/Info';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  @Input() info: Info;
  constructor() { }

  ngOnInit() {
  }

}
