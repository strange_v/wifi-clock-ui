import { Component, OnInit, Input } from '@angular/core';
import { SystemSettings } from '../../models/SystemSettings';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-system-settings',
  templateUrl: './system-settings.component.html',
  styleUrls: ['./system-settings.component.scss']
})
export class SystemSettingsComponent implements OnInit {
  @Input()
  public data: SystemSettings;
  @Input()
  public parent: FormGroup;

  constructor() {
  }

  ngOnInit() {
    this.parent.registerControl('wifiSsid', new FormControl(this.data.wifiSSID, Validators.required));
    this.parent.registerControl('wifiPwd', new FormControl(this.data.wifiPwd));
    this.parent.registerControl('authUser', new FormControl(this.data.authUser, Validators.required));
    this.parent.registerControl('authPwd', new FormControl(this.data.authPwd));
    this.parent.registerControl('otaPwd', new FormControl(this.data.otaPwd));
  }
}
