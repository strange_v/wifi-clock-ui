import { Component, ViewEncapsulation, OnInit, Input } from '@angular/core';
import { Settings } from '../../models/Settings';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TabsComponent implements OnInit {
  @Input() settings: Settings;
  form: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({});
  }

  getValues() {
    return this.form.getRawValue();
  }

  isValid() {
    return this.form.valid;
  }

}
