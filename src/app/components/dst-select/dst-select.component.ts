import { Component, EventEmitter, forwardRef, OnInit, Output, Input } from '@angular/core';
import { AbstractControl, ControlValueAccessor, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors, Validator, ValidatorFn, FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { TimeChangeRule } from '../../models/TimeChangeRule';

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DstSelectComponent),
  multi: true
};
export const CUSTOM_INPUT_CONTROL_VALIDATORS: any = {
  provide: NG_VALIDATORS,
  useExisting: forwardRef(() => DstSelectComponent),
  multi: true,
};

@Component({
  selector: 'app-dst-select',
  templateUrl: './dst-select.component.html',
  styleUrls: ['./dst-select.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR, CUSTOM_INPUT_CONTROL_VALIDATORS]
})
export class DstSelectComponent implements OnInit, ControlValueAccessor, Validator {
  @Input() label: Text;
  @Input() abbr: string;
  @Input() disabled: boolean;
  public form: FormGroup;
  private innerValue: TimeChangeRule;
  private _onChange: any;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.innerValue = new TimeChangeRule;
    this.innerValue.abbrev = this.abbr;

    this.form = this.fb.group({
      week: new FormControl(this.innerValue.week, Validators.required),
      dow: new FormControl(this.innerValue.dow, Validators.required),
      month: new FormControl(this.innerValue.month, Validators.required),
      hour: new FormControl(this.innerValue.hour, Validators.compose([
        Validators.required,
        Validators.min(0),
        Validators.max(23),
      ])),
    });
  }

  writeValue(value: TimeChangeRule): void {
    if (value == null || value == this.innerValue)
      return;

    this.innerValue = value;
    this.form.controls['week'].setValue(value.week);
    this.form.controls['dow'].setValue(value.dow);
    this.form.controls['month'].setValue(value.month);
    this.form.controls['hour'].setValue(value.hour);
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  validate(c: AbstractControl): ValidationErrors {
    return this.form.controls['hour'].errors;
  }

  registerOnValidatorChange?(fn: () => void): void {
  }

  setDisabledState(isDisabled: boolean): void {
    const state = isDisabled ? 'disable' : 'enable';

    this.form.controls['week'][state]();
    this.form.controls['dow'][state]();
    this.form.controls['month'][state]();
    this.form.controls['hour'][state]();
  }

  onWeekChange(value: any) {
    this.innerValue.week = value;
    this._onChange(this.innerValue);
  }
  onDowChange(value: any) {
    this.innerValue.dow = value;
    this._onChange(this.innerValue);
  }
  onMonthChange(value: any) {
    this.innerValue.month = value;
    this._onChange(this.innerValue);
  }
  onHourChange(value: number) {
    this.innerValue.hour = value;
    this._onChange(this.innerValue);
  }
}
