import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { DisplaySettings } from '../../models/DisplaySettings';
import { Time } from 'src/app/models/Time';

@Component({
  selector: 'app-display-settings',
  templateUrl: './display-settings.component.html',
  styleUrls: ['./display-settings.component.scss']
})
export class DisplaySettingsComponent implements OnInit {
  @Input()
  public data: DisplaySettings;
  @Input()
  public parent: FormGroup;

  constructor() { }

  ngOnInit() {
    const blinkColumn = new FormControl(this.data.blinkColumn, Validators.required);
    //blinkColumn.registerOnChange(this.blinkColumnChange);

    this.parent.registerControl('leadingZero', new FormControl(this.data.leadingZero, Validators.required));
    this.parent.registerControl('blinkColumn', blinkColumn);
    this.parent.registerControl('doNotBlink', new FormControl(this.data.doNotBlink, Validators.required));
    this.parent.registerControl('dnbFrom', new FormControl(this.timeString(this.data.dnbFrom), Validators.required));
    this.parent.registerControl('dnbTo', new FormControl(this.timeString(this.data.dnbTo), Validators.required));
    this.parent.registerControl('minBrightness', new FormControl(this.data.minBrightness, [
      Validators.required,
      Validators.min(1),
      Validators.max(4000)
    ]));
    this.parent.registerControl('maxBrightness', new FormControl(this.data.maxBrightness, [
      Validators.required,
      Validators.min(1),
      Validators.max(4000)
    ]));
  }

  blinkColumnChange() {
    const state = this.parent.controls['blinkColumn'].value ? 'enable' : 'disable';
    
    this.parent.controls['doNotBlink'][state]();
    this.parent.controls['dnbFrom'][state]();
    this.parent.controls['dnbTo'][state]();
  }

  doNotBlinkChange() {
    const state = this.parent.controls['doNotBlink'].value ? 'enable' : 'disable';
    
    this.parent.controls['dnbFrom'][state]();
    this.parent.controls['dnbTo'][state]();
  }

  timeString(time: Time): string {
    if (!time)
      return '';

    return `${time.hour}:${time.minute}`;
  }
}
