import { TimeChangeRule } from "./TimeChangeRule"

export class TimeSettings {
    ntpUrl: string;
    syncPeriod: number;
    offset: number;
    useDST: boolean;
    startDST: TimeChangeRule;
    endDST: TimeChangeRule;
}
