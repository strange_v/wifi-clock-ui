import { Time } from "./Time";

export class DisplaySettings {
    leadingZero: boolean;
    blinkColumn: boolean;
    doNotBlink: boolean;
    dnbFrom: Time;
    dnbTo: Time;
    minBrightness: number;
    maxBrightness: number;
}
