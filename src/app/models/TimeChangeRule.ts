export class TimeChangeRule {
    abbrev: string;
    week: number;
    dow: number;
    month: number;
    hour: number;
}
