export class SystemSettings {
    wifiSSID: string;
    wifiPwd: string;
    authUser: string;
    authPwd: string;
    otaPwd: string;
}
