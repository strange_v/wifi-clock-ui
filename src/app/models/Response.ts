export class Response<T> {
    public code: number;
    public entity: T;
}