import { SystemSettings } from "./SystemSettings";
import { TimeSettings } from "./TimeSettings";
import { DisplaySettings } from "./DisplaySettings";
import { Info } from "./Info";

export class Settings {
    info: Info;
	system: SystemSettings;
	time: TimeSettings;
	display: DisplaySettings;

	constructor() {
		this.system = new SystemSettings;
		this.time = new TimeSettings;
		this.display = new DisplaySettings;
	}
}
