'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());

const cfg = {
	info: {
		id: 145784,
		version: '1.x.x',
	},
	system: {
		wifiSSID: 'SSID',
		wifiPwd: '        ',
		authUser: 'admin',
		authPwd: '        ',
		otaPwd: '        '
	},
	time: {
		ntpUrl: 'pool.ntp.org',
		syncPeriod: 300,
		offset: 120,
		useDST: true,
		startDST: {
			week: 0,
			dow: 7,
			month: 3,
			hour: 2
		},
		endDST: {
			week: 0,
			dow: 7,
			month: 3,
			hour: 2
		}
	},
	display: {
		leadingZero: true,
		blinkColumn: true,
		doNotBlink: true,
		dnbFrom: {
			hour: 22,
			minute: 45
		},
		dnbTo: {
			hour: 8,
			minute: 15
		},
		minBrightness: 25,
		maxBrightness: 230
	}
};

app.get('/api/config', (req, res) => {
	res.send({
		code: 0,
		entity: cfg
	});
});
app.post('/api/config', (req, res) => {
	console.log(req.body);
	res.send({
		code: 0
	});
});


app.listen(3000, () => {
	console.log('Example app listening on port 3000!')
});
